import React from 'react';

export default class SearchBar extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            query: ""
        }
    }
    render(){
      return (
        <section className={`bg-white relative w-full border-b border-grey-light`}>
            <div className={`py-6 w-full pl-4 pr-20 md:pr-4 md:max-w-xs mx-auto`}>
                <input 
                type={`search`} 
                className={`font-sans px-2 pb-1 pt-2 leading-tight text-black text-lg border-b border-grey-light w-full outline-none focus:border-black`} 
                onChange={(event) => this.handleChange(event)}
                placeholder={`Search...`}
                />
            </div>
        </section>
      );
    }

    handleChange(event){
        this.setState({
            query: event.target.value
        })

        this.props.searchHandler(event.target.value)
    }
}