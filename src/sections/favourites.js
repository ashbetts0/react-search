import React from 'react';

export default class Favourites
 extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            show: false,
            favourites: this.props.favourites
        }
    }
    render(){
      return (
        <section 
        className={`w-8 h-full absolute pin-r pin-t flex items-center justify-center mr-4 md:mr-16 
        ${this.state.favourites.length > 0 ? 'cursor-pointer' : ''}`}
        onClick={(event) => this.toggleFavourites()}    
        >
            {this.state.favourites.length > 0 ? (
                <svg className={`w-full`} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M24.85 10.126c2.018-4.783 6.628-8.125 11.99-8.125 7.223 0 12.425 6.179 13.079 13.543 0 0 .353 1.828-.424 5.119-1.058 4.482-3.545 8.464-6.898 11.503L24.85 48 7.402 32.165c-3.353-3.038-5.84-7.021-6.898-11.503-.777-3.291-.424-5.119-.424-5.119C.734 8.179 5.936 2 13.159 2c5.363 0 9.673 3.343 11.691 8.126z" fill="#d75a4a"/></svg>
            ) : (
                <svg className={`w-full`} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 51.997 51.997"><path d="M51.911 16.242c-.759-8.354-6.672-14.415-14.072-14.415-4.93 0-9.444 2.653-11.984 6.905-2.517-4.307-6.846-6.906-11.697-6.906C6.759 1.826.845 7.887.087 16.241c-.06.369-.306 2.311.442 5.478 1.078 4.568 3.568 8.723 7.199 12.013l18.115 16.439 18.426-16.438c3.631-3.291 6.121-7.445 7.199-12.014.748-3.166.502-5.108.443-5.477zm-2.39 5.019c-.984 4.172-3.265 7.973-6.59 10.985L25.855 47.481 9.072 32.25c-3.331-3.018-5.611-6.818-6.596-10.99-.708-2.997-.417-4.69-.416-4.701l.015-.101c.65-7.319 5.731-12.632 12.083-12.632 4.687 0 8.813 2.88 10.771 7.515l.921 2.183.921-2.183c1.927-4.564 6.271-7.514 11.069-7.514 6.351 0 11.433 5.313 12.096 12.727.002.016.293 1.71-.415 4.707z"/></svg>
            )}
            <ul className={`${this.state.show ? 'flex flex-wrap' : 'hidden'} list-reset absolute pin-t pin-r z-20 mt-16 w-64 bg-white shadow max-h-half-screen overflow-scroll`}>
                <li className={`font-sans font-semibold text-left w-full text-black py-4 px-4 border-b border-grey cursor-default sticky pin-t bg-white`}>Watch List</li>
                {this.state.favourites.map((item, i) => {
                    return (
                        <li key={i} className={`w-full py-4 px-4 flex items-center justify-start cursor-default ${i < this.state.favourites.length -1 ? 'border-b border-grey-lighter' : ''}`}>
                            <span className={`min-w-4 flex items-center justify-center cursor-pointer`} onClick={(event) => this.removeFavourite(item)}>
                                <svg className={`w-4`}xmlns="http://www.w3.org/2000/svg" viewBox="0 0 212.982 212.982"><path d="M131.804 106.491l75.936-75.936c6.99-6.99 6.99-18.323 0-25.312-6.99-6.99-18.322-6.99-25.312 0L106.491 81.18 30.554 5.242c-6.99-6.99-18.322-6.99-25.312 0-6.989 6.99-6.989 18.323 0 25.312l75.937 75.936-75.937 75.937c-6.989 6.99-6.989 18.323 0 25.312 6.99 6.99 18.322 6.99 25.312 0l75.937-75.937 75.937 75.937c6.989 6.99 18.322 6.99 25.312 0 6.99-6.99 6.99-18.322 0-25.312l-75.936-75.936z" fillRule="evenodd" clipRule="evenodd"/></svg>
                            </span>
                            <p className={`pl-4 font-sans text-sm text-black`}>{item.split('/')[1]}</p>
                        </li>
                    )
                })}
            </ul>
        </section>
      );
    }

    componentWillReceiveProps(props){
        this.setState({
            show: props.favourites.length > 0 && this.state.show,
            favourites: props.favourites
        })
    }

    toggleFavourites(){
        this.setState({
            show: !this.state.show && this.state.favourites.length > 0
        })
    }

    removeFavourite(item){
        let favourite = {"imdbID": (item.split('/')[0])}
        this.props.set(favourite)
    }
}