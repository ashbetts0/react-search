import React from 'react';
import * as Snippets from "../snippets";

export default class SearchResults extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            results: this.props.results
        }
    }

    componentWillReceiveProps(props){
        this.setState({
            results: props.results
        })
    }

    render(){
        return (
            <section className={`bg-grey-lighter h-full container mx-auto relative min-h-full w-full py-8`}>
                <div className={`px-4`}>
                {this.state.results.Response === "True" ? (
                    <div className={`flex items-start flex-wrap justify-start`}>
                        {this.state.results.Search.map((result, i) => {
                            return (
                                <Snippets.ResultItem key={i} result={result} set={this.props.set} favourites={this.props.favourites} />
                            )
                        })}
                    </div>
                ) : (
                    <h1 className={`font-sans font-semibold text-lg text-black`}>
                        {this.state.results ? 
                            (this.state.results.Error.includes('Too many') ? 'Refine search, too many results' : 
                                (this.state.results.Error.includes('Movie not found') ? 'No Results Found' : 'Use the search bar above to begin')
                            )
                        : 
                            'Use the search bar above to begin'
                        }
                    </h1>
                )}
                </div>
            </section>
        );
    }
}