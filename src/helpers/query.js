export default async function Query(string, type){

    const apiKey = 'cccbe2e2';
    const url = `http://www.omdbapi.com/?apikey=${apiKey}&${type}=`

    return (
        fetch(url + string)
        .then(response => response.json())
        .then(data => {
            return data
        })
        .catch(error => {
            console.log(error)
            return 'Sorry, no results found.'
        })
        
    )
}


