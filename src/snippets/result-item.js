import React from 'react';

export default class ResultItem extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            result: this.props.result,
            favourite: this.props.favourites.filter((item) => item.includes(this.props.result.imdbID)).length > 0
        }
    }

    componentWillReceiveProps(props){
        this.setState({
            result: props.result,
            favourite: props.favourites.filter((item) => item.includes(props.result.imdbID)).length > 0
        })
    }

    render(){
        return (
        <div className={`w-1/2 md:w-1/3 lg:w-1/4 p-2`}>
            <div className={`py-6 px-4 max-w-xs mx-auto bg-white hover:shadow relative`}>
                <div
                className={`w-full pt-full bg-cover bg-center bg-no-repeat mb-4`} 
                style={{backgroundImage: "url('" + this.state.result.Poster + "')"}} />
                <div className={`w-full flex items-start justify-start relative`}>
                    <p className={`font-sans text-black pr-12`}>
                        {this.state.result.Title}
                    </p>
                    <span className={`w-8 absolute pin-t pin-r cursor-pointer z-10`} onClick={(event => this.handleClick(this.state.result))}>
                        {this.state.favourite ? (
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50"><path d="M24.85 10.126c2.018-4.783 6.628-8.125 11.99-8.125 7.223 0 12.425 6.179 13.079 13.543 0 0 .353 1.828-.424 5.119-1.058 4.482-3.545 8.464-6.898 11.503L24.85 48 7.402 32.165c-3.353-3.038-5.84-7.021-6.898-11.503-.777-3.291-.424-5.119-.424-5.119C.734 8.179 5.936 2 13.159 2c5.363 0 9.673 3.343 11.691 8.126z" fill="#d75a4a"/></svg>
                        ) : (
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 51.997 51.997"><path d="M51.911 16.242c-.759-8.354-6.672-14.415-14.072-14.415-4.93 0-9.444 2.653-11.984 6.905-2.517-4.307-6.846-6.906-11.697-6.906C6.759 1.826.845 7.887.087 16.241c-.06.369-.306 2.311.442 5.478 1.078 4.568 3.568 8.723 7.199 12.013l18.115 16.439 18.426-16.438c3.631-3.291 6.121-7.445 7.199-12.014.748-3.166.502-5.108.443-5.477zm-2.39 5.019c-.984 4.172-3.265 7.973-6.59 10.985L25.855 47.481 9.072 32.25c-3.331-3.018-5.611-6.818-6.596-10.99-.708-2.997-.417-4.69-.416-4.701l.015-.101c.65-7.319 5.731-12.632 12.083-12.632 4.687 0 8.813 2.88 10.771 7.515l.921 2.183.921-2.183c1.927-4.564 6.271-7.514 11.069-7.514 6.351 0 11.433 5.313 12.096 12.727.002.016.293 1.71-.415 4.707z"/></svg>
                        )}
                    </span>
                </div>
            </div>
        </div>
        );
    }

    handleClick(result){
        this.props.set(result);
    }
}