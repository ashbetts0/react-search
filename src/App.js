import React from 'react';
import './index.css'
import * as Pages from "./pages"

export default function App() {

  return (
    <div className={`min-h-screen bg-grey-lighter`}>
      <Pages.Home />
    </div>
  );

}