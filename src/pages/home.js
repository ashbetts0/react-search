import React from 'react';
import * as Sections from "../sections";
import * as Helpers from "../helpers";

export default class Home extends React.Component {

  constructor(props){
    super(props);

    this.state = {
        query: "",
        response: "",
        favourites: this.getFavourites()
    }

    this.handleSearch = this.handleSearch.bind(this);
    this.getFavourites = this.getFavourites.bind(this);
    this.setFavourites = this.setFavourites.bind(this);
  }

  render(){
    return (
      <React.Fragment>
        <header className={`relative`}>
          <Sections.SearchBar searchHandler={this.handleSearch} />
          <Sections.Favourites favourites={this.state.favourites} set={this.setFavourites}/>
        </header>
        <main className={``}>
          <Sections.SearchResults results={this.state.response} set={this.setFavourites} favourites={this.state.favourites}/>
        </main>
      </React.Fragment>
    )
  }

  async handleSearch(query){
      
    let response = await Helpers.Query(query, 's');

    this.setState({
      query: query,
      response: response
    })  
  }

  getFavourites(){
    return window.localStorage.getItem('favourites') ? window.localStorage.getItem('favourites').split(',') : []
  }

  setFavourites(item){
    let favourites = this.state.favourites;

    if(favourites && favourites.filter((favourite) => favourite.includes(item.imdbID)).length > 0){
      let index = favourites.findIndex(favourite => favourite.includes(item.imdbID));
      favourites.splice(index, 1);
    }
    else{
      favourites.push(item.imdbID + '/' + item.Title);
    }

    this.setState({
      favourites: favourites
    })

    window.localStorage.setItem('favourites', favourites);
  }
}



